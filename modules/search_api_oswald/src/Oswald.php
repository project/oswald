<?php

declare(strict_types=1);

namespace Drupal\search_api_oswald;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Oswald client class.
 */
class Oswald {
  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;
  /**
   * The url to Oswald.
   *
   * @var string
   */
  private $url = NULL;
  /**
   * The api key to connect to Oswald.
   *
   * @var string
   */
  private $api_key = NULL;

  /**
   * Set URL and Api Key.
   *
   * @param string $url
   *   The url to Oswald.
   * @param string $api_key
   *   The APi keuy for oswald.
   */
  public function setUrlApiKey(string $url, string $api_key) {
    $this->url = $url;
    $this->api_key = $api_key;
  }

  /**
   * Get the Oswald client.
   *
   * @return \GuzzleHttp\Client
   *   The http client.
   */
  public function getClient(): Client {
    if (!isset($this->httpClient)) {
      $options = [
        'headers' => [
          'Content-Type' => 'application/json',
          // We're not using the standard here 'Authorization' =>
          // 'Bearer ' . $this->api_key.
          'apiKey' => $this->api_key,
          'Accept' => 'application/json',
        ],
        'base_uri' => $this->url,
        'verify' => FALSE,
      ];

      $this->httpClient = new Client($options);
    }
    return $this->httpClient;
  }

  /**
   * Inserts or updates an array of vectors to Oswald.
   *
   * @param array $parameters
   *   An array.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The API response.
   */
  public function upsert(array $parameters): ResponseInterface {
    try {
      $this->delete([
        'source_ids' => [$parameters['metadata']['source_id']],
      ]);
    }
    catch (\Exception $exception) {
      // Do nothing, if it does not exist, carry on and try insert.
    }
    $payload = $parameters;
    return $this->getClient()
      ->post('insert', [
        'json' => $payload,
      ]);
  }

  /**
   * Delete records in Oswald.
   *
   * @param array $parameters
   *   An array with at least keys 'source_ids' and 'collection'.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response object.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function delete(array $parameters): ResponseInterface {
    if (empty($parameters['source_ids'])) {
      throw new \Exception('Source IDs to delete are required by Oswald');
    }
    $payload = [
      'ids' => $parameters['source_ids'],
    ];
    return $this->getClient()->post('delete', [
      'json' => $payload,
    ]);
  }

  /**
   * Delete all records in Oswald.
   *
   * @param array $parameters
   *   An array with at least key 'collection'.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function deleteAll(array $parameters): void {
    $this->dropAll();
  }

  /**
   * Drops a complete collection in Oswald.
   *
   * @param array $parameters
   *   The collection to delete vectors from.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response object.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function dropAll(array $parameters) {
    return $this->getClient()->post('drop');
  }

}
