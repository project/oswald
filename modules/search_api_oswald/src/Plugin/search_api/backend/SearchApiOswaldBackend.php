<?php

namespace Drupal\search_api_oswald\Plugin\search_api\backend;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_oswald\Oswald;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Oswald backend for search api.
 *
 * @SearchApiBackend(
 *   id = "search_api_oswald",
 *   label = @Translation("Oswald"),
 *   description = @Translation("Index items on Oswald.")
 * )
 */
class SearchApiOswaldBackend extends BackendPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * The Oswald client.
   */
  protected Oswald $client;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $client = new Oswald();
    $client->setUrlApiKey($this->configuration['webhook_url'] ?? '', $this->configuration['api_key'] ?? '');
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'webhook_url' => NULL,
      'api_key' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['webhook_info'] = [
      '#type' => 'markup',
      '#markup' => 'In Oswald create the knowledge base first.
<br>There you will find the Required URL and Token. Insert these below.
<br><br> This index should not be used as a search index, only to push data.
<br><br><b>Make sure to configure the following fields in the index:"url", "content" (the rendered page),  and "title"</b>',
      '#weight' => '-99',
    ];
    $form['webhook_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook url'),
      '#description' => $this->t('The oswald webhook url. eg. https://app.oswald.ai/api/v1/data-sources/:id/ (WITH trailing slash!)'),
      '#default_value' => $this->configuration['webhook_url'],
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('Your API key to use the Azure AI Search API.'),
      '#default_value' => $this->configuration['api_key'],
    ];

    $config = \Drupal::config('search_api_oswald.settings');
    $verbose = $config->get('verbose_logging');
    $verbose = ($verbose === NULL) ? FALSE : $verbose;
    $form['verbose'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verbose'),
      '#description' => $this->t('Log every event to watchdog, this is configurable in settings.php.<br>$config[\'search_api_oswald.settings\'][\'verbose_logging\'] = TRUE;'),
      '#value' => $verbose,
      '#default_value' => $verbose,
      '#disabled' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    $successfulItemIds = [];
    $itemList = [];
    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item) {
      try {
        $this->deleteItems($index, [$item->getId()]);
      }
      catch (\Exception $exception) {
      }
      $itemList[] = $item->getId();
      $params = [
        'metadata' => [
          'source_id' => $item->getId(),
        ],
      ];
      $data = [];
      /** @var \Drupal\search_api\Item\FieldInterface $field */
      foreach ($item as $field) {
        $field_type = $field->getType();
        if (!empty($field->getValues())) {
          $values = $this->buildFieldValues($field, $field_type);
          $data[$field->getFieldIdentifier()] = $values;
          if ($field->getFieldIdentifier() == 'title' || $field->getFieldIdentifier() == 'url') {
            $data[$field->getFieldIdentifier()] = $values[0];
          }
          if ($field->getFieldIdentifier() == 'content') {
            $data[$field->getFieldIdentifier()] = $values[0]->toText();
          }
        }
      }

      // Add our ID as id!
      $data['id'] = $item->getId();
      $data['content_type'] = 'html';

      if (!isset($data['title'])) {
        throw new SearchApiException('TITLE IS AN EXPECTED FIELD !!');
      }
      if (!isset($data['url'])) {
        throw new SearchApiException('URL IS AN EXPECTED FIELD !!');
      }
      if (!isset($data['content'])) {
        throw new SearchApiException('CONTENT IS AN EXPECTED FIELD!!');
      }
      $params['data'] = [];
      $params['data'][0] = $data;
      $this->getClient()->upsert($params);
      $successfulItemIds[] = $item->getId();
    }
    return $successfulItemIds;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    $this->getClient()->delete([
      'filter' => [
        'item_id' => ['$in' => array_values($item_ids)],
      ],
      'source_ids' => array_values($item_ids),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    try {
      $this->getClient()->dropAll([]);
    }
    catch (\Exception $e) {
      throw new SearchApiException(sprintf('An error occurred Dropping oswald data '), 0, $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    // Not used for now $index = $query->getIndex();
    if ($query->hasTag('server_index_status')) {
      return NULL;
    }
    // We wont be seaarching this index I think.
  }

  /**
   * Extract query metadata values to a result item.
   */
  public function extractMetadata(object $result_row, ItemInterface $item): void {
    $item->setExtraData('metadata', $result_row->metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function getClient() {
    if (!isset($this->client)) {
      $this->client = $this->createClient($this->configuration);
    }
    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDataType($type) {
    return in_array($type, $this->getDataTypes());
  }

  /**
   * {@inheritdoc}
   */
  public function getDataTypes() {
    return [
      'text',
      'string',
      'integer',
      'decimal',
      'double',
      'boolean',
      'date',
    ];
  }

  /**
   * Gets the API client.
   */
  public function createClient(array &$configuration) {
    $client = new Oswald();
    $client->setUrlApiKey($this->configuration['webhook_url'], $this->configuration['api_key']);
    return $client;
  }

  /**
   * Builds field values.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   The field.
   * @param string $field_type
   *   The field type.
   *
   * @return array
   *   The fields params.
   */
  public function buildFieldValues(FieldInterface $field, string $field_type): array {
    $values = [];
    foreach ($field->getValues() as $value) {
      if ($value instanceof TextValue) {
        $values[] = $value->toText();
        continue;
      }
      $values[] = match ($field_type) {
        'string' => (string) $value,
        'boolean' => (boolean) $value,
        default => $value,
      };
    }
    return $values;
  }

}
