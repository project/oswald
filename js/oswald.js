/**
 * @file
 * Attaches Oswald event listeners.
 */

function sendDataToOswald() {
    let dataToSend = {eventName: 'oswald-data'};
    document.getElementById('oswald-widget').contentWindow.postMessage(dataToSend, '*');
}

function openOswaldWidget() {
    let dataToSend = {eventName: 'open-oswald-iframe'};
    document.getElementById('oswald-widget-bubble').contentWindow.postMessage(dataToSend, '*');
}

function openOswaldWidgetByURLParameter(event) {
    if (event.data.eventName === 'oswald-widget-initialised') {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);

        if (urlParams.has('openOswaldWidget') && urlParams.get('openOswaldWidget') === 'true') {
            openOswaldWidget();
        }
    }
}

(function ($, Drupal, once) {
    'use strict';

    Drupal.behaviors.oswaldBindEventListener = {
        attach: function (context, settings) {
            setTimeout(function () {
                window.removeEventListener('message', sendDataToOswald);
                window.removeEventListener('message', openOswaldWidgetByURLParameter);
                window.addEventListener('message', sendDataToOswald);
                window.addEventListener('message', function (e) {
                    openOswaldWidgetByURLParameter(e);
                });
            }, 500);
        }
    };
})(jQuery, Drupal, once);
