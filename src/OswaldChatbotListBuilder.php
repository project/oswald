<?php

namespace Drupal\oswald;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build an overview of oswald_chatbot entities.
 *
 * @see \Drupal\oswald\Entity\OswaldChatbot
 */
class OswaldChatbotListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['oswald_chatbot_id'] = $this->t('Oswald chatbot id');
    $header['enabled'] = $this->t('Enabled');
    $header['environment'] = $this->t('Environment');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $oswald_chatbot) {
    /** @var \Drupal\oswald\Entity\OswaldChatbot $oswald_chatbot */
    $name = !empty($oswald_chatbot->get('name')->value) ? $oswald_chatbot->get('name')->value : $oswald_chatbot->id();
    $row['name'] = new Link($name, Url::fromRoute('entity.oswald_chatbot.canonical', ['oswald_chatbot' => $oswald_chatbot->id()]));
    $row['oswald_chatbot_id'] = $oswald_chatbot->get('oswald_chatbot_id')->value;
    $row['enabled'] = !empty($oswald_chatbot->get('enabled')->value) ? $this->t('Enabled') : $this->t('Disabled');
    $row['environment'] = $oswald_chatbot->get('environment')->value;

    return $row + parent::buildRow($oswald_chatbot);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $oswald_chatbot) {
    return parent::getDefaultOperations($oswald_chatbot);
  }

}
