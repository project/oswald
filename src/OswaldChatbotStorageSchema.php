<?php

namespace Drupal\oswald;

use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the oswald_chatbot schema handler.
 */
class OswaldChatbotStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();
    $index_fields = [];

    if (in_array($field_name, $index_fields, TRUE)) {
      $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
    }

    return $schema;
  }

}
