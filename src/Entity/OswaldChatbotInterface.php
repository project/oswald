<?php

namespace Drupal\oswald\Entity;

/**
 * Provides an interface for the Oswald chatbot entity.
 */
interface OswaldChatbotInterface {

  /**
   * Gets the Oswald URL for the configured environment.
   *
   * @return string
   *   The URL.
   */
  public function getUrlForEnvironment(): string;

  /**
   * Gets the Oswald chatbot ID.
   *
   * @return string
   *   The chatbot ID.
   */
  public function getChatbotId(): string;

  /**
   * Gets the boolean value of the auto open checkbox.
   *
   * @return string
   *   String value indicating if the chatbot should automatically open.
   */
  public function getAutoOpen(): string;

  /**
   * Gets the boolean value of the auto open mobile checkbox.
   *
   * @return string
   *   String value indicating if the chatbot should be prevented from
   *   opening on mobile.
   */
  public function getPreventAutoOpenMobile(): string;

}
