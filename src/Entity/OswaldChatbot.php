<?php

namespace Drupal\oswald\Entity;

use Detection\MobileDetect;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the oswald_chatbot entity.
 *
 * @ingroup oswald_chatbot
 *
 * @ContentEntityType(
 *   id = "oswald_chatbot",
 *   label = @Translation("Oswald Chatbot"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\oswald\OswaldChatbotListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "storage_schema" = "Drupal\oswald\OswaldChatbotStorageSchema",
 *     "form" = {
 *       "default" = "Drupal\oswald\Form\OswaldChatbotForm",
 *       "add" = "Drupal\oswald\Form\OswaldChatbotForm",
 *       "edit" = "Drupal\oswald\Form\OswaldChatbotForm",
 *       "delete" = "Drupal\oswald\Form\OswaldChatbotDeleteForm",
 *     },
 *     "access" = "Drupal\oswald\OswaldChatbotAccessControlHandler",
 *   },
 *   base_table = "oswald_chatbot",
 *   admin_permission = "administer oswald chatbot configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/oswald/bots/{oswald_chatbot}",
 *     "edit-form" = "/admin/config/oswald/bots/{oswald_chatbot}/edit",
 *     "test-form" = "/admin/config/oswald/bots/{oswald_chatbot}/test",
 *     "delete-form" = "/admin/config/oswald/bots/{oswald_chatbot}/edit",
 *     "collection" = "/admin/config/oswald/bots"
 *   },
 * )
 */
class OswaldChatbot extends ContentEntityBase implements OswaldChatbotInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDefaultValue('')
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['oswald_chatbot_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bot id'))
      ->setDescription(t('The ID of the Oswald chatbot widget. Can be found under Integrations » API » Click on "API Token"'))
      ->setDefaultValue('')
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['enabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enabled'))
      ->setRequired(FALSE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['auto_open'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Automatically open'))
      ->setDescription(t('Enabling this checkbox will ensure the widget opens automatically without any user interaction.'))
      ->setRequired(FALSE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['prevent_auto_open_mobile'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Prevent automatic opening on mobile devices'))
      ->setDescription(t("Enabling this checkbox will ensure the widget won't open automatically on mobile devices."))
      ->setRequired(FALSE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['environment'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Environment'))
      ->setRequired(TRUE)
      ->setDefaultValue('prod')
      ->setSetting('allowed_values', [
        'acc' => 'Acceptance',
        'prod' => 'Production',

      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'list_default',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['request_path'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Request Path'))
      ->setDefaultValue('')
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_textfield',
        'weight' => 2,
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 2,
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlForEnvironment(): string {
    $environment = $this->get('environment')->value;
    if ($environment === 'acc') {
      return 'https://api-acc.oswald.ai';
    }

    return 'https://api.oswald.ai';
  }

  /**
   * {@inheritdoc}
   */
  public function getChatbotId(): string {
    return trim($this->get('oswald_chatbot_id')->value);
  }

  /**
   * {@inheritdoc}
   */
  public function getAutoOpen(): string {
    if ($this->get('auto_open')->value) {
      return ($this->isMobileRequest() && $this->getPreventAutoOpenMobile() == 'true') ? 'false' : 'true';
    }

    return 'false';
  }

  /**
   * {@inheritdoc}
   */
  public function getPreventAutoOpenMobile(): string {
    if ($this->hasField('prevent_auto_open_mobile') && $this->get('prevent_auto_open_mobile')->value) {
      return 'true';
    }

    return 'false';
  }

  /**
   * Determine if the request is made from a mobile device.
   *
   * @return bool
   *   Returns true if the request is made from a mobile device.
   */
  protected function isMobileRequest(): bool {
    /** @var \Symfony\Component\HttpFoundation\RequestStack $request_stack */
    $request_stack = \Drupal::service('request_stack');
    $request = $request_stack->getCurrentRequest();

    $detect = new MobileDetect();
    $detect->setUserAgent($request->headers->get('User-Agent'));

    return $detect->isMobile();
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags() {
    $cache_tags = parent::getCacheTags();

    if ($this->getPreventAutoOpenMobile() == 'true') {
      $cache_tags[] = 'oswald_chatbot.prevent_auto_open:1';
      $cache_tags[] = 'oswald_chatbot.is_mobile:' . $this->isMobileRequest();
    }
    else {
      $cache_tags[] = 'oswald_chatbot.prevent_auto_open:0';
    }

    return $cache_tags;
  }

}
