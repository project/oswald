<?php

namespace Drupal\oswald\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\oswald\Entity\OswaldChatbot;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class OswaldChatbotController.
 *
 * Controller to add & initialize the Oswald chatbot widget.
 */
class OswaldChatbotController {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected ConditionManager $manager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The module handler interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected Session $session;

  /**
   * OswaldChatbotController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager interface.
   * @param \Drupal\Core\Executable\ExecutableManagerInterface $manager
   *   The ConditionManager for building the visibility UI.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match interface.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface.
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   *   The session.
   */
  public function __construct(EntityTypeManager $entity_type_manager, ExecutableManagerInterface $manager, LanguageManagerInterface $language_manager, RouteMatchInterface $route_match, AccountProxyInterface $current_user, ModuleHandlerInterface $module_handler, Session $session) {
    $this->entityTypeManager = $entity_type_manager;
    $this->manager = $manager;
    $this->languageManager = $language_manager;
    $this->routeMatch = $route_match;
    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
    $this->session = $session;
  }

  /**
   * Attaches the Oswald chatbot widget and initializes it.
   *
   * @param array $page
   *   The whole page array.
   */
  public function attachAndInitialize(array &$page): void {
    $chatbot = $this->getChatbot();
    // Exit early if we have not received a chatbot.
    if (!$chatbot) {
      return;
    }

    // Set necessary caching information.
    $cacheable_metadata = CacheableMetadata::createFromRenderArray($page);
    $cacheable_metadata->addCacheTags($chatbot->getCacheTags());
    $cacheable_metadata->applyTo($page);

    if (!$chatbot->get('enabled')->value) {
      return;
    }

    // Generate the snippet to attach.
    $script = 'setTimeout(function(){  var e=new XMLHttpRequest;if(e.open("GET","' . $chatbot->getUrlForEnvironment() . '/api/v1/chats/' . $chatbot->getChatbotId() . '/widget?open=' . $chatbot->getAutoOpen() . '&locale=' . $this->languageManager->getCurrentLanguage()->getId() . '",!1),e.send(null),200==e.status){var t=document.createElement("script");t.innerHTML=e.responseText,document.body.appendChild(t);};}, 500);';
    $page['#attached']['html_head'][] = [
      [
        '#type' => 'html_tag',
        '#tag' => 'script',
        '#value' => $script,
      ],
      'oswald-embed',
    ];
    $page['#attached']['library'][] = 'oswald/oswald.event_listener';
  }

  /**
   * Get the active chatbot.
   *
   * @return \Drupal\oswald\Entity\OswaldChatbot|null
   *   The chatbot to activate, NULL if no active chatbots found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Executable\ExecutableException
   */
  protected function getChatbot(): ?OswaldChatbot {
    if ($this->routeMatch->getRouteName() === 'entity.oswald_chatbot.test_form') {
      // Only load this specific bot and return.
      return OswaldChatbot::load($this->routeMatch->getParameter('oswald_chatbot'));
    }

    // Get all the enabled chatbots.
    $chatbots = $this->entityTypeManager->getStorage('oswald_chatbot')->loadMultiple();

    /** @var \Drupal\system\Plugin\Condition\RequestPath $condition */
    $condition = $this->manager->createInstance('request_path');

    // Remove chatbots which do not evaluate our conditions.
    /** @var \Drupal\oswald\Entity\OswaldChatbot $chatbot */
    foreach ($chatbots as $key => $chatbot) {
      $condition->setConfiguration(json_decode($chatbot->get('request_path')->value, TRUE));
      if (!$this->manager->execute($condition)) {
        unset($chatbots[$key]);
      }
    }

    // Allow other modules to alter the active chatbot.
    if (count($chatbots) >= 1) {
      $data = ['route' => $this->routeMatch->getRouteName(), 'bots' => $chatbots, 'user' => $this->currentUser];
      $this->moduleHandler->alter('oswald_active_bot', $data);
      if (isset($data['bots'])) {
        return reset($data['bots']);
      }
    }

    return NULL;
  }

}
