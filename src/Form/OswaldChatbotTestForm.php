<?php

namespace Drupal\oswald\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OswaldChatbotTestForm for testing the chatform.
 */
class OswaldChatbotTestForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private EntityTypeManager $entityTypeManager;

  /**
   * OswaldChatbotTestForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oswald_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $params = \Drupal::routeMatch()->getParameters()->all();
    $bot_id = $params['oswald_chatbot'];
    $controller = $this->entityTypeManager->getStorage('oswald_chatbot');
    /** @var \Drupal\oswald\Entity\OswaldChatbot $oswald_chatbot */
    $oswald_chatbot = $controller->load($bot_id);

    $form['oswald_test'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Testing Oswald chatbot: %name', [
        '%name' => $oswald_chatbot->get('name')->value,
      ]),
    ];

    $admin_link = Link::fromTextAndUrl($this->t('Oswald overview page'), Url::fromRoute('entity.oswald_chatbot.collection'))->toString();

    $form['oswald_test']['testing_info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Back to your @here.', ['@here' => $admin_link]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['oswald.general_settings'];
  }

}
