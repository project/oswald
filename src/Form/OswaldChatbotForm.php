<?php

namespace Drupal\oswald\Form;

use Drupal\Component\Plugin\Factory\FactoryInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Plugin\Condition\RequestPath;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;

/**
 * Form controller for oswald_chatbot edit forms.
 */
class OswaldChatbotForm extends ContentEntityForm {

  /**
   * The request_path condition plugin.
   *
   * @var \Drupal\system\Plugin\Condition\RequestPath
   */
  protected RequestPath $condition;

  /**
   * Constructs a OswaldChatbotForm object.
   *
   * {@inheritdoc}
   *
   * @param \Drupal\Component\Plugin\Factory\FactoryInterface $plugin_factory
   *   The factory interface.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, FactoryInterface $plugin_factory) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->condition = $plugin_factory->createInstance('request_path');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.condition')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    // Set the default condition configuration.
    $entity = $this->entity;
    if ($request_path = $entity->get('request_path')->value) {
      $this->condition->setConfiguration(json_decode($request_path, TRUE));
    }
    // Build the condition configuration form.
    $form += $this->condition->buildConfigurationForm($form, $form_state);
    // Put the elements to the bottom of the form.
    $form['pages']['#weight'] = 20;
    $form['negate']['#weight'] = 20;
    // Only show mobile option if auto open is active.
    $form['prevent_auto_open_mobile']['#states'] = [
      'visible' => [
        ':input[name="auto_open[value]"]' => [
          'checked' => TRUE,
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->condition->submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);
    $account = \Drupal::currentUser();
    $element['delete']['#access'] = $account->hasPermission('administer oswald chatbot configuration');
    $element['delete']['#weight'] = 100;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    // Set the condition configuration.
    $entity->set('request_path', json_encode($this->condition->getConfiguration()));
    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Oswald chatbot.', [
          '%label' => $entity->get('name')->value,
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Oswald chatbot.', [
          '%label' => $entity->get('name')->value,
        ]));
        break;
    }

    $form_state->setRedirect('entity.oswald_chatbot.collection');
  }

}
