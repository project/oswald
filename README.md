# Oswald chatbot

The Oswald chatbot module embeds the chatbot widget from https://bothive.be and allows you to configure it.

In order for you to use this module you'll need to have a valid API key from https://www.oswald.ai.

## Installation.


Install with composer: ```composer require 'drupal/oswald:^4.0@alpha'```

- Go to you modules overview page.
- Enable the `Oswald chatbot` module.

## Configuration


- Navigate to /admin/config/oswald/bots
- Click on `Add Oswald chatbot`.
  - Give it a proper name.
  - Enter the chatbot id. Can be found under Integrations » API » Click on "API Token".
  - Enable or disable the chatbot.
  - Optionally enable the auto open function of the chatbot.
  - Select environment "production" unless instructed otherwise.
  - Define the display condition.

You can now in the bots overview "test" your bot.
If your bot is enabled and if it meets the display condition, it will be shown.


## Extend

In `oswald.api.php` you can find an example hook implementation on how to alter the active chatbot with custom code.

```php
function hook_oswald_active_bot_alter(&$data) {
  foreach ($data['bots'] as $key => $chatbot) {
    if ($data['route'] == 'my_custom_route') {
      // Select this bot, leave it in the array.
    }
    else {
      unset($data['bots'][$key]);
    }
  }
}
```

Implement the `hook_oswald_active_bot_alter` hook to customize which bot should be shown on which url.
Make sure the code is lightweight as this hook is called on every request.

Some example implementations:
- Show a different bot for every role.
- Show a different bot on different sections of the site
- Show different bots on some but not other pages.
- ...

If multiple bots are left in the array, the first one will be selected as the array will be reset. It's only possible to render one chatbot on one page.
