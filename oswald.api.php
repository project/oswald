<?php

/**
 * @file
 * Hooks provided by the Oswald chatbot module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the available chatbots before page load.
 *
 * Modules may implement this hook to select which chatbot should be active on
 * which page. The implementing module should only leave the 1 chatbot in the
 * array that should be shown. If multiple bots are left in the array, the first
 * one will be selected as the array will be reset. It's only possible to render
 * one chatbot on one specific page.
 *
 * The list will only contain chatbots which are enabled and which passed the
 * conditions. It is possible there is only one chatbot left in the array after
 * the conditions have been checked.
 *
 * @param array $data
 *   This parameter contains the enabled chatbots, the user object and the
 *   current route.
 */
function hook_oswald_active_bot_alter(&$data) {
  foreach ($data['bots'] as $key => $chatbot) {
    if ($data['route'] === 'my_custom_route') {
      // Select this bot, leave it in the array.
    }
    else {
      unset($data['bots'][$key]);
    }
  }
}

/**
 * @} End of "addtogroup hooks".
 */
